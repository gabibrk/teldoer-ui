import { FormControl } from '@angular/forms';

export function imbdLinkValidator(control: FormControl) {
    let link = control.value;
    if (link) { 
      if (!link.startsWith("imdb.com") && !link.startsWith("https://www.imdb.com")) {
        return { linkValid: true }
      }
      return null;
    }
  }

export function imgLinkValidator(control: FormControl) {
    let link = control.value;
    if (link) {
      if (!link.endsWith(".jpg")) {
        return { imgValid: true }
      }
      return null;
    }
  }