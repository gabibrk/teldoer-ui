import { Component, OnInit } from '@angular/core';
import { MoviesService } from './services/movies.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit {
  logged: boolean;

  constructor(private movieService:MoviesService ) { }

  ngOnInit(): void { 
    this.movieService.user.subscribe(res=>{
      console.log(res); 
      if(res.length>0){
        this.logged = true;
        console.log("app "  + this.logged);
      }
    })
  } 
  title = 'movies-gui';

  toggle(){}
}
