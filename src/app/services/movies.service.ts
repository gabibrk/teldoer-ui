import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders  } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs'; 

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private userName = new Subject<string>();
  user = this.userName.asObservable();

  setUserLogin(name: any) {
    console.log(name);
    this.userName.next(name);
  }

  
  private movieCategory = new Subject();
  categoryArr = this.movieCategory.asObservable();
  
  private showCategory = new Subject();
  showCategoryS = this.showCategory.asObservable();

  baseUrl='http://localhost:3000/'
  constructor(private http:HttpClient) { }

   private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
   private options = {
    headers: this.headers
  }

  getMovies( ): Observable<any> {
    const url = this.baseUrl+"movies";
    return this.http.get(url)
    // .pipe( map(reponse => reponse.json())
  }

  addNewMovie(movie):Observable<any>{
    const url = this.baseUrl+"reg";
    return this.http.post(url, movie ,this.options);
  }

  sendCategoryData(tempArr: Array<String>) {
    this.movieCategory.next(tempArr);
  }
  
  setCategoryData(String) {
    this.showCategory.next(String);
  }


}
