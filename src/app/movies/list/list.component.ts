import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {MoviesService} from './../../services/movies.service' ;
import {imbdLinkValidator, imgLinkValidator} from './../../Utils/validators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { from } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  newMovieForm: FormGroup

  movies: any[];
  filteredMovies: any[];

  movieCategory:String[]=[];
  constructor(private movieService:MoviesService) {
    
   }

  ngOnInit(): void {
    this.newMovieForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      category: new FormControl('', Validators.required),
      link: new FormControl('', [Validators.required, imbdLinkValidator]),
      image: new FormControl('', [Validators.required, imgLinkValidator])
    });

    this.movies = this.baseMovies.movies;
    // this.movieService.getMovies().subscribe(res=>{
      // console.log(res);
      // this.movies=res; 
      // this.filteredMovies=res;
      this.movies.forEach(movie=>{
        this.movieCategory.includes(movie.category)? null: this.movieCategory.push(movie.category);
      })
      this.movieService.sendCategoryData(this.movieCategory);
      this.movieService.showCategoryS.subscribe(res=>{
       this.filterByCategory(res);
    })
      console.log(this.movieCategory);
    // })
   
  }

  addToMovieList(movie){ 
    // this.movieService.addNewMovie(movie).subscribe(res=>{
    //     console.log(res);
    //     if(res) this.movies.push(movie);
    // })
    // this.newMovieForm.reset();
    console.log(movie);
    this.movies.push(movie);
    this.movieCategory.includes(movie.category)? null: this.movieCategory.push(movie.category);
  }

  filterByCategory(category){
    this.filteredMovies=this.movies;
    this.filteredMovies=this.filteredMovies.filter(movie=>movie.category == category);
  }


  baseMovies = {
    movies : [
    {
         name : 'Harry Potter and the Sorcerers Stone',
         category : 'Adventure' , 
         link : 'https://www.imdb.com/title/tt0241527/?ref_=nv_sr_srsg_0' , 
         image : 'https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_UX182_CR0,0,182,268_AL_.jpg%22'
    },
    {
      name : 'Harry Potter and the Goblet of Fire',
      category : 'Fantasy' , 
      link : 'https://www.imdb.com/title/tt0330373/?ref_=nv_sr_srsg_7' , 
      image : 'https://m.media-amazon.com/images/M/MV5BMTI1NDMyMjExOF5BMl5BanBnXkFtZTcwOTc4MjQzMQ@@._V1_UX182_CR0,0,182,268_AL_.jpg'
    },
    {
         name : 'The Lord of the Rings: The Return of the King',
         category : 'Adventure' , 
         link : 'https://www.imdb.com/title/tt0167260/?ref_=vp_vi_tt' , 
         image : 'https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg'
    },
    ]
  }


}

