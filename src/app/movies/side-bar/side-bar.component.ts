import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MoviesService } from 'src/app/services/movies.service';


export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})


export class SideBarComponent implements OnInit {
  movieCategory:any;
  userLoggedIn: String;
  logged:Boolean;
  ngOnInit(): void {
    this.movieService.categoryArr.subscribe(res=>{
        this.movieCategory=res;
    })
    this.movieService.user.subscribe(res=>{
      console.log(res);
      this.userLoggedIn = res;
      if(res.length>0){
        this.logged = true;
        console.log(this.logged);
      }
    })
  }

  animal: string;
  name: string;

  constructor( private movieService:MoviesService) { }


  filterCategory(category){
    console.log(category);
    this.movieService.setCategoryData(category);
  }

}