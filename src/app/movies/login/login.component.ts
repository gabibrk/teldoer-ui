import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'; 
import { MoviesService } from 'src/app/services/movies.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  constructor(private movieSrv:MoviesService, private router:Router) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      name: new FormControl('',),
      pass: new FormControl('', ),
    });
  }


  login(form){
    console.log(form);
    if(form.name=='admin' && form.pass=='1234'){
        this.movieSrv.setUserLogin(form.name);
        this.router.navigateByUrl('list');
    }

  }
}
